Two quick scripts to demonstrate linux | characters and how they interact with stdin/stdout/stderr. Nothing special or fancy, mostly for class.

    ./output.py
    output.py's STDERR(2): about to write 'this is output.py's stdout(1)' to STDOUT(1)
    
    this is output.py's stdout(1)

    ./output.py | ./input.py
    output.py's STDERR(2): about to write 'this is output.py's stdout(1)' to STDOUT(1)
    
    
    
    input.py's STDOUT(1): This program's STDIN(0) is: this is output.py's stdout(1)
